#!/usr/bin/env pythoh3

'''
Cálculo del número óptimo de árboles.
'''

import sys


def compute_trees(trees):
    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production


def compute_all(min_trees, max_trees):
    productions = []
    for x in range(min_trees,
                   max_trees + 1):  # Para cada número de árboles añadimos un número de producción en la lista
        productions.append((x, compute_trees(x)))

    return productions


def read_arguments():
    if len(sys.argv) != 6:  # Si el usuario introduce número incorrecto de argumentos, salimos
        sys.exit("Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>")

    try:  # Extraemos cada argumento con su variable como un entero
        base_trees = int(sys.argv[1])
        fruit_per_tree = int(sys.argv[2])
        reduction = int(sys.argv[3])
        min = int(sys.argv[4])
        max = int(sys.argv[5])
    except ValueError:  # Si alguno de los argumentos no es entero, salimos
        sys.exit("All arguments must be integers")

    return base_trees, fruit_per_tree, reduction, min, max


def main():
    global base_trees, fruit_per_tree, reduction
    base_trees, fruit_per_tree, reduction, min, max = read_arguments()
    productions = compute_all(min, max)

    production_maxima = 0
    trees = 0

    for x in range(0, len(productions)):
        print(productions[x][0], productions[x][1]) #Imprime cada producción y número de árboles
        if productions[x][1] > production_maxima:
            production_maxima = productions[x][1] #Guarda la producción máxima hasta el momento
            trees = productions[x][0]

    print(f"Best production: {production_maxima}, for {trees} trees")


if __name__ == '__main__':
    main()
